#include <iostream>
#include <string>
#include "Space.h"
#include "Code.h"
#include "Result.h"
using namespace std;


int main()
{
	
	Space mSpace;
	Code mGuess;
	Result::RESULT mResult;
	cout<<"Please input the secret for guess : ";
	string secret;
	int a, b, c, d;
	do
	{
		cout<<"( 4 numbers with no repeat )"<<endl;
		cin>>secret;
		a = secret[0] - '0';
		b = secret[1] - '0';
		c = secret[2] - '0';
		d = secret[3] - '0';
		
	}while( secret.size() != 4 || !Code::isLegal( a, b, c, d ) );
	
	
	Code theSecret( a, b, c, d );

	int steps = 0;
	while( !mSpace.isEmpty() )
	{// if the space is not empty
		// keep looping until find the correct code
		// choose a guess from the original space
		steps ++;
		cout<<"step : "<<steps<<endl;
		if( steps == 1 )
			mGuess = Code( 0, 1, 2, 3 );
		else
			mGuess = mSpace.chooseAGuess();
		cout<<"guess : ";
		mGuess.print();
		mResult = Result::judge( mGuess, theSecret );
		cout<<"result : ";
		Result::print( mResult );
		// check if the guess is correct
		if( mResult == Result::RESULT::RESULT_40 )
		{// you guess the right code !!
			cout<<"You guess the right code !";
			cout<<"The code is : ";
			mGuess.print();
			cout<<"....."<<endl;
			cout<<"Steps you used : "<<steps<<"....."<<endl;
			break;
		}
		else
		{// keep guess
			// use the result and the guess to update the space
			mSpace.update( mGuess, mResult );
		}
	}
	string enter;
	cin>>enter;
	return 0;
}
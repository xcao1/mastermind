#include "Result.h"

Result::Result()
{
}
Result::~Result()
{
}



bool Result::isLegal( int (&result)[2] )
{
	// 1.both B and W should range from 0 - 4
	for( int i = 0; i != 2; i ++ )
	{
		if( result[0] < 0 || result[1] > 4 )
		{
			cerr<<"error : result illegal";
			return false;
		}
	}
	// 2.B + W should range from 0 to 4
	int sum = result[0] + result[1];
	if( sum > 4 )
	{
		cerr<<"error : result illegal";
		return false;
	}
	return true;
}

Result::RESULT Result::convertToRESULT( int (&result)[2] )
{
	if( !isLegal( result ) )
	{
		cerr<<"result illegal, can not convert";
	}
	// convert
	if( result[0] == 4 )
	{
		return RESULT_40;
	}
	else if( result[0] == 3 )
	{
		return RESULT_30;
	}
	else if( result[0] == 2 )
	{
		switch( result[1] )
		{
		case 0:
			return RESULT_20;
			break;
		case 1:
			return RESULT_21;
			break;
		case 2:
			return RESULT_22;
			break;
		default:
			cerr<<"only can be 20,21,22";
			break;
		}
	}
	else if( result[0] == 1 )
	{
		switch( result[1] )
		{
		case 0:
			return RESULT_10;
			break;
		case 1:
			return RESULT_11;
			break;
		case 2:
			return RESULT_12;
			break;
		case 3:
			return RESULT_13;
			break;
		default :
			cerr<<"only can be 10,11,12,13";
			break;
		}
	}
	else if( result[0] == 0 )
	{
		switch( result[1] )
		{
		case 0:
			return RESULT_00;
			break;
		case 1:
			return RESULT_01;
			break;
		case 2:
			return RESULT_02;
			break;
		case 3:
			return RESULT_03;
			break;
		case 4:
			return RESULT_04;
			break;
		default :
			cerr<<"only can be 00,01,02,03,04";
			break;
		}
	}
	else
	{
		cerr<<"B can only be 0,1,2,3,4";
	}
}




Result::RESULT Result::judge( Code guess, Code code )
{
	// check if the guess and the code is legal
	if( !guess.isLegal() || !code.isLegal() )
	{
		cerr<<"illegal guess or code";
	}
	// B : result[0], W : result[1]
	int result[2] = { 0, 0 };
	// 1. check how many numbers in guess exist in the code
	for( int i = 0; i != 4; i ++ )
	{// for each number in guess
		for( int j = 0; j != 4; j ++ )
		{// for each number in code
			if( guess.code[i] == code.code[j] )
				result[1]++;
		}
	}
	// 2. check how many numbers in guess is in the correct position
	for( int i = 0; i != 4; i ++ )
	{
		if( guess.code[i] == code.code[i] )
		{
			result[0]++;
			result[1]--;
		}
	}
	// 3. check if the result_BW is legal
	if( !isLegal( result ) )
	{
		cerr<<"illegal result";
	}
	// 4. convert to enum RESULT
	return convertToRESULT( result );
}

bool Result::sameResult( Code guess, Code code, RESULT result )
{
	if( result == judge( guess, code ) )
		return true;
	else 
		return false;
}

void Result::print( RESULT r )
{
	switch( r )
	{
	case RESULT_00:
		cout<<"B0W0"<<endl;
		break;
	case RESULT_01:
		cout<<"B0W1"<<endl;
		break;
	case RESULT_02:
		cout<<"B0W2"<<endl;
		break;
	case RESULT_03:
		cout<<"B0W3"<<endl;
		break;
	case RESULT_04:
		cout<<"B0W4"<<endl;
		break;
	case RESULT_10:
		cout<<"B1W0"<<endl;
		break;
	case RESULT_11:
		cout<<"B1W1"<<endl;
		break;
	case RESULT_12:
		cout<<"B1W2"<<endl;
		break;
	case RESULT_13:
		cout<<"B1W3"<<endl;
		break;
	case RESULT_20:
		cout<<"B2W0"<<endl;
		break;
	case RESULT_21:
		cout<<"B2W1"<<endl;
		break;
	case RESULT_22:
		cout<<"B2W2"<<endl;
		break;
	case RESULT_30:
		cout<<"B3W0"<<endl;
		break;
	case RESULT_40:
		cout<<"B4W0"<<endl;
		break;
	default:
		cerr<<"wrong result";
		break;
	}
}
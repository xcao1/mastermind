#include "Space.h"

Space::Space()
{
	for( int a = 0; a != 10; a ++ )
	{
		for( int b = 0; b != 10; b ++ )
		{
			for( int c = 0; c != 10; c ++ )
			{
				for( int d = 0; d != 10; d ++ )
				{
					if( Code::isLegal( a, b, c, d ) )
					{
						Code temp( a, b, c, d );
						space.push_back( temp );
#ifdef SEARCH_IN_ORIGINAL_SPACE
						originalBackUp.push_back( temp );
#endif
					}
				}
			}
		}
	}
}
Space::~Space()
{
}

void Space::update( Code guess, Result::RESULT result )
{
	// a vector used to store the new space
	vector<Code> mNewSpace;
	// traverse the whole space
	vector<Code>::iterator codes_iter = space.begin();
	while( codes_iter != space.end() )
	{
		// find out codes in space that have the same result
		if( Result::sameResult( guess, *codes_iter, result ) )
		{
			// push them into the new space vector
			mNewSpace.push_back( *codes_iter );
		}
		// go to next code
		codes_iter ++;
	}
	space = mNewSpace;
}

int Space::scoreOfResult( Code guess, Result::RESULT result )
{
	// for this guess
	// how many codes in the space
	// can give the same result
	int sum = 0;
	if( !space.empty() )
	{// if the space is not empty
		// traverse each code in the space
		// to find out if the code can give same result
		vector<Code>::iterator codes_iter = space.begin();
		while( codes_iter != space.end() )
		{
			if( Result::sameResult( guess, *codes_iter, result ) )
			{// if same
				sum ++;
			}
			// go to next
			codes_iter ++;
		}
	}
	/*cout<<"result : "<<result<<"      "<<sum<<endl;*/
	return sum;
}
int Space::scoreOfSUM( Code guess )
{
	int sum = 0;
	for( Result::RESULT i = Result::RESULT::RESULT_00; i != Result::RESULT( Result::RESULT::RESULT_40 + 1 ); i = Result::RESULT (i + 1) )
	{// traverse for each result
		sum = sum + scoreOfResult( guess, i );
	}
	/*cout<<"                                                        sum : "<<sum<<endl;*/
	cout<<".";
	return sum;
}
int Space::scoreOfWorest( Code guess )
{
	vector<int> scores;
	for( Result::RESULT i = Result::RESULT::RESULT_00; i != Result::RESULT( Result::RESULT::RESULT_40 + 1 ); i = Result::RESULT (i + 1) )
	{// traverse for each result
		int score = scoreOfResult( guess, i );
		scores.push_back( score );
	}
	int index = 0;// begin + index point to the current one
	int theWorestOne = index;// begin + theworest point to the worest one
	// compare to find out the worest score of this guess
	vector<int>::iterator scores_iter = scores.begin();
	while( ( scores_iter + 1 ) != scores.end() )
	{
		// compare each score
		if( *scores_iter < *( scores_iter + 1 ) )
		{
			// the next one is worse than this one
			theWorestOne = index + 1;
		}
		// go to next
		scores_iter ++;
		index ++;
	}
	// get the worest one
	scores_iter = scores.begin();
	scores_iter += theWorestOne;
	/*cout<<"                                           the worest score : "<<*scores_iter<<endl;*/
	return *scores_iter;
}
Code Space::chooseAGuess()
{

	Code *theBestGuess;
	// traverse codes in the original space
	//if( !originalBackUp.empty() )
	if( !space.empty() )
	{
		vector<int> scores;		
#ifdef SEARCH_IN_ORIGINAL_SPACE
		vector<Code>::iterator codes_iter = originalBackUp.begin();
		while( codes_iter != originalBackUp.end() )
#endif
#ifdef SEARCH_IN_SPACE
		vector<Code>::iterator codes_iter = space.begin();
		while( codes_iter != space.end() )
#endif
		{
			/*cout<<"                     code : ";
			(*codes_iter).print();
			cout<<"  "<<endl;*/
			// the less the sum is,
			// the better the guess will be
#ifdef USE_SUM
			int sum = scoreOfSUM( *codes_iter );
			scores.push_back( sum );
#endif
#ifdef USE_WOREST
			int worest = scoreOfWorest( *codes_iter );
			scores.push_back( worest );
#endif
			// go to next
			codes_iter ++;
		}
		// compare each score
		vector<int>::iterator scores_iter = scores.begin();
		int index = 0;
		
		int theSmallestSocreNO = 0;
		while( ( scores_iter + 1 ) != scores.end() )
		{
			if( *scores_iter > *( scores_iter + 1 ) )
			{
				theSmallestSocreNO = index + 1;
			}
			scores_iter ++;
			index ++;
		}
#ifdef SEARCH_IN_ORIGINAL_SPACE
		codes_iter = originalBackUp.begin();
#endif
#ifdef SEARCH_IN_SPACE
		codes_iter = space.begin();
#endif
		theBestGuess = &(*( codes_iter + theSmallestSocreNO ));
	}
	return *theBestGuess;
}


bool Space::isEmpty()
{
	if( space.empty() )
		return true;
	else
		return false;
}
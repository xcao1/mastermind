#include "Code.h"

Code::Code()
{
	for( int i = 0; i != 4; i ++ )
	{
		code[i] = 0;
	}
}
Code::Code( int a, int b, int c, int d )
{
	code[0] = a;
	code[1] = b;
	code[2] = c;
	code[3] = d;
}

Code::~Code()
{
}

bool Code::isLegal( int (&c)[4] )
{
	for( int i = 0; i != 4; i ++ )
	{
		if( c[i] < 0 || c[i] > 9 )
		{// illegal guess
			//cerr<<"error : should range from 0 to 9 .";
			return false;
		}
		int left = 3 - i;
		for( int j = 1; j != (left+1); j ++ )
		{
			if( c[i] == c[i+j] )
			{
				//cerr<<"error : the number should not repeat .";
				return false;
			}
		}
	}
	// if nothing wrong, return true
	return true;
}
bool Code::isLegal( int a, int b, int c, int d )
{
	int temp[4] = { a, b, c, d };
	return isLegal( temp );
}
bool Code::isLegal()
{
	return isLegal( code );
}

bool Code::sameWith( Code c )
{
	if( code[0] == c.code[0] && code[1] == c.code[1] && code[2] == c.code[2] && code[3] == c.code[3] )
		return true;
	else 
		return false;
}

void Code::print()
{
	cout<<"("<<code[0]<<","<<code[1]<<","<<code[2]<<","<<code[3]<<")";
}
#ifndef _CODE_H_
#define _CODE_H_
#include <iostream>

using namespace std;
class Code
{
public:
	int code[4];

	Code();
	Code( int a, int b, int c, int d );
	
	~Code();

	// check if the code is legal
	// the number should not repeat
	// since this kind of code(the answer) is not allowed
	bool isLegal();
	static bool isLegal( int (&c)[4] );
	static bool isLegal( int a, int b, int c, int d );

	bool sameWith( Code c );
	
	void print(); 

};


#endif
#ifndef _SPACE_H_
#define _SPACE_H_

#include <iostream>
#include <vector>
#include "Code.h"
#include "Result.h"
using namespace std;
class Space
{
//#define SEARCH_IN_ORIGINAL_SPACE
#define SEARCH_IN_SPACE

//#define USE_WOREST
#define USE_SUM
private:
	vector<Code> space;
#ifdef SEARCH_IN_ORIGINAL_SPACE
	vector<Code> originalBackUp;
#endif
public:
	Space();
	~Space();
	
	bool isEmpty();

	// keep the code that have the same result, delete other codes from space
	void update( Code guess, Result::RESULT result );
	// a score of each result
	int scoreOfResult( Code guess, Result::RESULT result );
	// score of sum
	int scoreOfSUM( Code guess );
	// score of worest
	int scoreOfWorest( Code guess );
	// choose a guess
	Code chooseAGuess();
	
};


#endif
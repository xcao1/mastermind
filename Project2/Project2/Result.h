#ifndef _RESULT_H_
#define _RESULT_H_

#include <iostream>
#include "Code.h"
using namespace std;
class Result
{
public:
	static enum RESULT 
	{ 
		RESULT_00, RESULT_01, RESULT_02, RESULT_03, RESULT_04,
		RESULT_10, RESULT_11, RESULT_12, RESULT_13, 
		RESULT_20, RESULT_21, RESULT_22,
		RESULT_30,
		RESULT_40
	};
	Result();
	~Result();

	static bool isLegal( int (&result)[2] );

	// judge want result will be
	static RESULT judge( Code guess, Code code );
	static bool sameResult( Code guess, Code code, RESULT result );

	static void print( RESULT r );
	
private:
	
	static RESULT convertToRESULT( int (&result)[2] );
	
	
};


#endif